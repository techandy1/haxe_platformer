package;

import flixel.FlxSprite;
import flixel.FlxState;
import flixel.addons.editors.tiled.TiledMap;
import flixel.addons.editors.tiled.TiledObjectLayer;
import flixel.group.FlxGroup.FlxTypedGroup;

class LevelState extends FlxState
{
	function createLevel()
	{
		final map = new TiledMap("assets/data/level1.tmx");
		final groundLayer:TiledObjectLayer = cast(map.getLayer("ground"));
		final groundGrp = new FlxTypedGroup<FlxSprite>();

		for (ground in groundLayer.objects)
		{
			final groundSprt = new FlxSprite(ground.x, ground.y);
			groundSprt.loadGraphic("assets/images/groundTiles.png", false, 600, 400);
			groundGrp.add(groundSprt);
		}

		add(groundGrp);
	}
}
