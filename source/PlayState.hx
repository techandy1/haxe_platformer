package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.tile.FlxTilemap;

class PlayState extends LevelState
{
	public var map:FlxTilemap;
	public var player:FlxSprite;

	override public function create()
	{
		createLevel();
		player = new Player(48, 16);

		add(player);

		super.create();
	}

	override public function update(elapsed:Float)
	{
		super.update(elapsed);

		FlxG.collide(map, player);
	}
}
